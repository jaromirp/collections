package com.sda.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Zad2b {

    //public class Main {
    public static void main(String[] args) {
        /*
        Stwórz oddzielnego maina, a w nim kolejną listę integerów. Wykonaj zadania:
        - dodaj do listy 10 liczb losowych
        - oblicz sumę elementów na liście (wypisz ją)
        - oblicz średnią elementów na liście (wypisz ją)
        - podaj medianę elementów na liście (wypisz ją) (Collections.sort( listaDoPosortowania) - sortuje listę)

            (przykład użycia Collections.sort(lista):
            ArrayList<Integer> liczby = new ArrayList<>();
            liczby.add(5);
            liczby.add(10);
            liczby.add(15);

            Collections.sort(liczby);

                - znajdź największy oraz najmniejszy element na liście (znajdź go posługując się pętlą for) - wypisz indeks elementu
                - znajdź największy oraz najmniejszy element na liście (znajdź go pętlą for, a następnie określ index posługując się metodą indexOf)
            Sprawdź działanie aplikacji.
         */

        List<Integer> list = new ArrayList<>();
        Random generator = new Random();
        for (int i = 0; i < 10; i++) {
            list.add(generator.nextInt(100));
        }

        double suma = 0;
//        for (int i = 0; i < list.size(); i++) {
//            suma+=list.get(i);
//        }
        for (Integer wartosc : list) {
            suma += wartosc;
        }
        System.out.println("Suma: " + suma);
        System.out.println("Średnia: " + (suma / list.size()));

        // tworze kopie elementow
        List<Integer> kopia = new ArrayList<>(list);

        // sortuje elementy
        Collections.sort(list);

        // środkowy element - nieparzysta ilosc elementow
        if (list.size() % 2 == 1) {
            System.out.println("Mediana: " + list.get(list.size() / 2));
        } else if (list.size() % 2 == 0) {
            System.out.println("Mediana: " + ((list.get(list.size() / 2) + list.get((list.size() / 2) - 1)) / 2.0));
        }

        System.out.println(list);
        int min = list.get(0); // lista jest posortowana, najmniejszy element jest 0
        int max = list.get(list.size() - 1); // lista jest posortowana, najwiekszy element jest ostatni

        System.out.println("max: " + max);
        System.out.println("min: " + min);

        //index of

        int indexOfMax = kopia.indexOf(max);
        int indexOfMin = kopia.indexOf(min);

        System.out.println("Orginalna tablica: " + kopia);
        System.out.println("Index of max: " + indexOfMax);
        System.out.println("Index of min: " + indexOfMin);
    }
}

