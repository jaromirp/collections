//package com.sda.collections.lista.zad5;

package com.sda.collections.lista.zad5;

public class Student implements Comparable<Student> {
    private String name;
    private String surname;
    private Plec plec;
    private String index;

    public Student(String name, String surname, Plec plec, String index) {
        this.name = name;
        this.surname = surname;
        this.plec = plec;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    @Override
    public int compareTo(Student o) {
        int index1 = Integer.parseInt(getIndex());
        int index2 = Integer.parseInt(o.getIndex());
        if (index1 > index2) {
            return 1; // index 1 jest wiekszy od index 2
        } else if (index1 < index2) {
            return -1; // index 2 jest wiekszy od index 1
        }
        // równe
        return 0;
    }

    public enum Plec {
        KOBIETA,
        MEZCZYZNA
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", plec=" + plec +
                ", index='" + index + '\'' +
                '}';
    }
}