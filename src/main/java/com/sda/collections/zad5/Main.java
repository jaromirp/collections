package com.sda.collections.zad5;

//5. Stwórz klasę Student która posiada pola:
//nr indeksu
//imie
//nazwisko
//płeć (wartość enum)
//
//Stwórz instancję kolekcji typu ArrayList, która zawiera obiekty klasy Student
//w mainie dodaj do kolekcji kilku studentów (dodaj je ręcznie - wpisz cokolwiek).
//
//a. Spróbuj wypisać elementy listy stosując zwykłego "sout'a"
//b. Spróbuj wypisać elementy stosując pętlę foreach
//c. Wypisz tylko kobiety
//d. Wypisz tylko mężczyzn
//e. Wypisz tylko indeksy osób

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<com.sda.collections.lista.zad5.Student> lista_studentow = new ArrayList<>(
                Arrays.asList(
                        new com.sda.collections.lista.zad5.Student("Michau", "Nijaki", com.sda.collections.lista.zad5.Student.Plec.MEZCZYZNA, "12345"),
                        new com.sda.collections.lista.zad5.Student("Marcin", "Taki", com.sda.collections.lista.zad5.Student.Plec.MEZCZYZNA, "12346"),
                        new com.sda.collections.lista.zad5.Student("Marian", "Inny", com.sda.collections.lista.zad5.Student.Plec.MEZCZYZNA, "12347"),
                        new com.sda.collections.lista.zad5.Student("Mirek", "Nieznany", com.sda.collections.lista.zad5.Student.Plec.KOBIETA, "1225"),
                        new com.sda.collections.lista.zad5.Student("Jurek", "Znany", com.sda.collections.lista.zad5.Student.Plec.KOBIETA, "12545"),
                        new com.sda.collections.lista.zad5.Student("Arek", "Pijany", com.sda.collections.lista.zad5.Student.Plec.MEZCZYZNA, "12745"),
                        new com.sda.collections.lista.zad5.Student("Darek", "Lezacy", com.sda.collections.lista.zad5.Student.Plec.MEZCZYZNA, "1845")
                )
        );

        // a
        System.out.println(lista_studentow);

        // b
        for (com.sda.collections.lista.zad5.Student student : lista_studentow) {
            System.out.println(student);
        }

        // c
        for (com.sda.collections.lista.zad5.Student student : lista_studentow) {
            if (student.getPlec() == com.sda.collections.lista.zad5.Student.Plec.KOBIETA) {
                System.out.println("Jestem kobieta: " + student);
            }
        }

        // d
        for (int i = 0; i < lista_studentow.size(); i++) {
            if (lista_studentow.get(i).getPlec() == com.sda.collections.lista.zad5.Student.Plec.MEZCZYZNA) {
                System.out.println("Jestem mezczyzna: " + lista_studentow.get(i));
            }
        }

        // e
        for (com.sda.collections.lista.zad5.Student studencik : lista_studentow) {
            System.out.println("Indeks : " + studencik.getIndex());
        }

    }
}



//Add CommentCollapse