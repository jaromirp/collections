package com.sda.collections;

import java.util.ArrayList;
import java.util.List;

public class Przyklad2 {
    //public class Przyklad {
    public static void main(String[] args) {
//        ArrayList<Integer> lista = new ArrayList<>();
//        List<Integer> lista = new LinkedList<>();
        List<Integer> lista = new ArrayList<>();

        // dodawanie elementów
        // dodanie elementu na koniec
        lista.add(213);
        // dodanie elementu na index 0
        lista.add(0, 214);
        lista.add(0);

        // listy o wiele łatwiej się wypisuje!
        System.out.println(lista);

        System.out.println(lista.get(0));
        lista.remove(0);
        System.out.println(lista.get(0));
//        lista.remove((Object)0);
//        System.out.println(lista.get(2));


        System.out.println(lista);

        int[] tab = new int[5];
        for (int i = 0; i < tab.length; i++) {
            System.out.println(tab[i]);
        }

        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i));
        }

//        lista.set(0, 512);

        // indexElementu = 0
        // indexElementu = -1
        int indexElementu = lista.indexOf(512);
        if (lista.contains(512)) {
            System.out.println("zawiera element 512");
        }


    }
}




