package com.sda.collections;

//2.
//Stwórz oddzielnego maina, a w nim kolejną listę integerów. Wykonaj zadania:
//    - dodaj do listy 10 liczb losowych
//    - oblicz sumę elementów na liście (wypisz ją)
//    - oblicz średnią elementów na liście (wypisz ją)
//    - podaj medianę elementów na liście (wypisz ją) (Collections.sort( listaDoPosortowania) - sortuje listę)

//(przykład użycia Collections.sort(lista):
//ArrayList<Integer> liczby = new ArrayList<>();
//liczby.add(5);
//liczby.add(10);
//liczby.add(15);
//
//Collections.sort(liczby);
//
//    - znajdź największy oraz najmniejszy element na liście (znajdź go posługując się pętlą for) - wypisz indeks elementu
//    - znajdź największy oraz najmniejszy element na liście (znajdź go pętlą for, a następnie określ index posługując się metodą indexOf)
//Sprawdź działanie aplikacji.

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Zad2 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(); // to musisz zadeklarować dla lista.add Kolekcje

        Random random = new Random();

        int suma =0;
        int srednia = 0;
        //int listaDoPosortowania = 0;

        for (int i = 0; i < 10; i++) {
            int wylosowanaLiczba = random.nextInt(); // wylosowana liczba
            lista.add(wylosowanaLiczba);

            System.out.println(wylosowanaLiczba);

            //listaDoPosortowania = (lista.collection.sort);
            Collections.sort(lista);

            suma = suma + wylosowanaLiczba;


            System.out.println();

        }
        srednia = (suma/lista.size()); //średnia
        System.out.println(suma);
        System.out.println(srednia);

        //Collections.sort(list); Policzenie Mediany
        if (lista.size() % 2 == 0) {
            double mediana = ((lista.get(lista.size() / 2)) + (double) (lista.get(lista.size() / 2 - 1))) / 2;
            System.out.println(mediana);
        } else {
            double mediana = lista.get(lista.size() / 2);
            System.out.println(mediana);
        }
    }




}
