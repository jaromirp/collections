package com.sda.collections.set;

//Umieść wszystkie elementy tablicy {10,12,10,3,4,12,12,300,12,40,55} w zbiorze (HashSet) oraz:
//Wypisz liczbę elementów na ekran (metoda size())
//Wypisz wszystkie zbioru elementy na ekran (forEach)
//Usuń elementy 10 i 12, wykonaj ponownie podpunkty a) i b)
//zmień implementacje na TreeSet

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Zad1Set {
    public static void main(String[] args) {
        Set<Integer> integers = new HashSet<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));
        System.out.println(integers);        //wyświetla elementy bez zdublowania
        System.out.println(integers.size()); //wyświetla liczbę elementów

        for (Integer integer: integers) {  //Wypisz wszystkie zbioru elementy na ekran (forEach)
            System.out.println(integer);

        }
        integers.remove(10);
        integers.remove(12);
        //integers.removeAll(Arrays.asList(10,12)); // lub tak możemy zapisać

        System.out.println(integers);        //wyświetla elementy bez zdublowania
        System.out.println(integers.size()); //wyświetla liczbę elementów

        //zmień implementacje na TreeSet

    }
}


