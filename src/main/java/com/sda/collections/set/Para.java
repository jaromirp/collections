package com.sda.collections.set;

public class Para {

    private int lewy;
    private int prawy;

    public Para(int lewy, int prawy) {
        this.lewy = lewy;
        this.prawy = prawy;
    }

    @Override
    public String toString() {
        return "Para{" +
                "lewy=" + lewy +
                ", prawy=" + prawy +
                '}';
    }
}



