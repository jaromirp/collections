package com.sda.collections.Lambda;

public class Main {
    public static void main(String[] args) {

        Instrument instrument = new Drum();
        instrument.makeSound();

        Instrument instrument2 = new Instrument() {
            @Override
            public void makeSound() {
                System.out.println("bdoink");

            }
        };

        instrument2.makeSound();

        Instrument instrument3 = () -> System.out.println("doink");
        instrument3.makeSound();

    }

    ;
}

