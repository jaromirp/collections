package com.sda.collections.Lambda.Zadania;

import java.util.function.Predicate;

public class PredicateMain {

    public static void main(String[] args) {
        Predicate<String> hasLetterA = new Predicate<String>() {
            @Override
            public boolean test(String text) {
                return text.toLowerCase().contains("a");
            }
        };
        Predicate<String> hasLetterA2 = (x) -> x.toLowerCase().contains("a");
        System.out.println(hasLetterA.test("jakis text"));
        System.out.println(hasLetterA2.test("jakis text"));

    }
}
