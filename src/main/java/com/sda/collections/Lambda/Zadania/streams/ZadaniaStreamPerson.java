package com.sda.collections.Lambda.Zadania.streams;

import com.sda.collections.Lambda.Zadania.Zad3kolejne.Person;

import java.util.*;
import java.util.stream.Collectors;

//1.Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale.
//Mając listę osób i korzystając ze streamów:
//a) uzyskaj listę mężczyzn
//b) uzyskaj listę dorosłych kobiet
//c) uzyskaj Optional<Person> z dorosłym Jackiem
//d) uzyskaj listę wszystkich nazwisk osób, które są w przedziale wiekowym: 15-19
//e)* uzyskaj sumę wieku wszystkich osób
//f)* uzyskaj średnią wieku wszystkich mężczyzn
//g)** znajdź nastarszą osobę w liście

public class ZadaniaStreamPerson {

    public static void main(String[] args) {
        Person person1 = new Person("Krzysztof", "Krawczyk", 45, true);
        Person personJacek = new Person("Jacek", "Krawczyk", 12, true);
        Person person2 = new Person("Baba", "Odchlopa", 46, false);
        Person person3 = new Person("Mlody", "Ziomek", 14, true);
        List<Person> peopleList = Arrays.asList(person1, person2, person3, personJacek);
        //List<Person> peopleList2 = Arrays.asList( personJacek,person3);

        //poniżej opis przykładowy Stream'a
        //1.Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale.
        //Mając listę osób i korzystając ze streamów:
        List<Person> onlyWomen = peopleList.stream().filter(x -> !x.isMale())
                .collect(Collectors.toList());
        System.out.println(onlyWomen);

        //a) uzyskaj listę mężczyzn
        List<Person> onlyMen = peopleList.stream().filter(x -> x.isMale())
                .collect(Collectors.toList());
        System.out.println(onlyMen);

        //b) uzyskaj listę dorosłych kobiet
        List<Person> onlyAdultWomnen2 = peopleList.stream()
                .filter(x -> !x.isMale())
                .filter(x -> x.getAge() > 17)
                .collect(Collectors.toList());

        //c) uzyskaj Optional<Person> z dorosłym Jackiem
        Optional<Person> adultJackOptional = peopleList.stream()
                .filter(x -> x.getAge() >= 18 && x.getFirstName().equals("Jacek")) //predykat sprawdza czy jest True lub FALSE
                .findAny();
        System.out.println(adultJackOptional);

        List<Integer> years = peopleList.stream().map(x -> x.getAge()).collect(Collectors.toList());
        System.out.println(years);

        //d) uzyskaj listę wszystkich nazwisk osób, które są w przedziale wiekowym: 15-19
        List<String> lastNamesOf15to19yo = peopleList.stream().filter(x -> x.getAge() >= 15)
                .filter(x -> x.getAge() <= 19)
                .map(x -> x.getLastName())
                .collect(Collectors.toList());

        System.out.println(lastNamesOf15to19yo);

        //e)* uzyskaj sumę wieku wszystkich osób

        List<Integer> ageOfAllPersons = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);  //peopleList.stream().filter()
        Integer sum1 = ageOfAllPersons.stream().mapToInt(x -> x).sum();
        Integer sum2 = ageOfAllPersons.stream().reduce(0, (x, y) -> x + y);

        System.out.println(sum1 + sum2);


        //f)* uzyskaj średnią wieku wszystkich mężczyzn
        OptionalDouble optAfv = peopleList.stream().filter(x -> x.isMale())
                .map(x -> x.getAge())
                .mapToDouble(x -> x)
                .average();

        if ( optAfv.isPresent()) {
            System.out.println(optAfv.getAsDouble());
        } else {
            System.out.println("Przecież nie ma nikogo w liście!");

            //g)** znajdź nastarszą osobę w liście

            //Optional<Person> oldestOpt = peopleList.stream()
            //g)** znajdź nastarszą osobę w liście
//            Comparator<Person> personComparator = new PersonComparator();
//            System.out.println(personComparator.compare(person1, person2));

            Comparator<Person> personComparatorWithLambda = (x, y) -> x.getAge() - y.getAge();
            Optional<Person> oldestOpt = peopleList.stream().max((x, y) -> x.getAge() - y.getAge());
            System.out.println(oldestOpt);

        }
    }
}





