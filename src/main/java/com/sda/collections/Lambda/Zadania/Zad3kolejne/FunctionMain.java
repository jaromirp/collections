package com.sda.collections.Lambda.Zadania.Zad3kolejne;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class FunctionMain {
    public static void main(String[] args) {
        Person person1 = new Person("Krzysztof", "Krawczyk", 45, true);
        Person personJacek = new Person("Jacek", "Krawczyk", 12, true);
        Person person2 = new Person("Baba", "Odchlopa", 46, false);
        Person person3 = new Person("Mlody", "Ziomek", 15, true);
        List<Person> peopleList = Arrays.asList(person1, person2, person3, personJacek);

        /*
        FUNCTION
Rozgrzewka: napisz funkcję, która przyjmuje listę intów i zwraca listę z samymi liczbami parzystymi
5. Napisz funkcję, która przyjmuje listę <Person> i zwraca listę z samymi kobietami.
6. Napisz funkcję, która przyjmuje Person i zwraca Student (stwórz taką klasę. Niech posiada student id), jeśli osoba ma mniej niż 18 lat. Student id wygeneruj losowo.
7. Napisz funkcję, która przyjmuje listę <Person> i zwraca listę studentów, korzystając z funkcji z poprzedniego zadania.
         */

        /*
        Rozgrzewka: napisz funkcję, która przyjmuje listę
         intów i zwraca listę z samymi liczbami parzystymi
         */
        List<Integer> numbersToCheck = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        Function<List<Integer>, List<Integer>> toEvenNumbersList = x -> {
            List<Integer> evenNumbers = new ArrayList<>();
            for (int number : x) {
                if (number % 2 == 0) evenNumbers.add(number);
            }
            return evenNumbers;
        };

        //5. Napisz funkcję, która przyjmuje listę <Person> i zwraca listę z samymi kobietami.

        Function<List<Person>, List<Person>> getOnlyWomen = people -> {
            List<Person> women = new ArrayList<>();
            for (Person person : people) {
                if (!person.isMale()) women.add(person);
            }
            return women;
        };

        /*
        6. Napisz funkcję, która przyjmuje Person i zwraca Student
         (stwórz taką klasę. Niech posiada student id), jeśli osoba ma mniej
         niż 18 lat. Student id wygeneruj losowo.
         */

        Function<Person, Student> toStudentIfNotAdult = x -> {
            Random random = new Random();
            if (x.getAge() < 18) {
                Student student = new Student(
                        x.getFirstName(),
                        x.getLastName(),
                        x.getAge(),
                        random.nextInt(1000)
                );

                return student;
            } else return null;

        };

        System.out.println(toStudentIfNotAdult.apply(person3));
        ;

/*
7. Napisz funkcję, która przyjmuje listę <Person>
i zwraca listę studentów, korzystając z funkcji z poprzedniego zadania.
 */
        Function<List<Person>, List<Student>> toStudentsIfNotAdults = x -> {

            List<Student> students = new ArrayList<>();

            for (Person person : x) {

                Student student = toStudentIfNotAdult.apply(person);
                if (student != null) students.add(student);
            }
            return students;
        };

        System.out.println(toStudentsIfNotAdults.apply(peopleList));

    }
}
