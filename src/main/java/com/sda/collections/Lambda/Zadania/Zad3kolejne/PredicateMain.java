package com.sda.collections.Lambda.Zadania.Zad3kolejne;

import java.time.Period;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PredicateMain {

    //  public class PredicateMain {

    public static void main(String[] args) {

        Predicate<String> hasLetterA = new Predicate<String>() {
            @Override
            public boolean test(String text) {
                return text.toLowerCase().contains("a");
            }
        };

        Predicate<String> hasLetterA2 = (x) -> x.toLowerCase().contains("a");
        System.out.println(hasLetterA.test("jakis text"));
        System.out.println(hasLetterA2.test("jkis text"));

        //1. Napisz funkcję, która przyjmuje liczbę i sprawdza, czy liczba ta jest parzysta.

        Predicate<Integer> isEvenPredicate = x -> x % 2 == 0;
        System.out.println(isEvenPredicate.test(3));
        System.out.println(isEvenPredicate.test(4));

        //2. Napisz funkcję, która przyjmuje listę Stringów i sprawdza, czy wśród nich jest String zawierający słowo "gotchya".
        List<String> lista = Arrays.asList("pierwsze zdanie;drugiezdanie;trzeciegotchyazdanie".split(";"));

        Predicate<List<String>> containsStringContainingGotchya = list -> {
            for (String element : list) {

                if (element.contains("gotchya")) return true;
            }
            return false;
        };


        System.out.println(containsStringContainingGotchya.test(lista));

        /*
        3. Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale. Napisz:

         */
        //	a) funkcję, która zwraca true, jeśli Person jest mężczyzną

        Person person1 = new Person("Krzysztof", "Krawczyk", 45, true);
        Person personJacek = new Person("Baba", "Odchlopa", 46, false);
        Person person2 = new Person("Mlody", "Ziomek", 15, true);
        Person person3 = new Person("Mlody", "Ziomek", 15, true);


        Predicate<Person> isMale = x -> x.isMale();

        // b) funkcję, któa zwraca true, jeśli Person ma na imię Jacek"
        String name = "JaCeK";
        Predicate<Person> hasNameJacek = x -> x.getFirstName().toLowerCase().equals("jacek");
        //to jest obiekt metoda -> Predicate<Person> hasNameJacek
                                      //to jest Lambda ten string -> x.getFirstName().toLowerCase().equals("jacek");
        System.out.println(hasNameJacek.test(personJacek));
        System.out.println(hasNameJacek.test(person1));

        // c) funkcje, która zwraca true, jeśli osoba jest pełnoletnia
        Predicate<Person> isAnAdult = x->x.getAge()>=18;

        //4.  Napisz funkcję, która zwraca true, jeśli w kolekcji <Person> znajduje się dorosły mężczyzna, który ma na imię jacek.
        Predicate<List<Person>> hasAnAdultJacek = x-> {
            for (Person person : x) {
                if(person.getAge()>=18&&person.getFirstName().toLowerCase().equals("jacek")){

                }
            }
            return false;
          };

    }
}
