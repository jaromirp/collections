package com.sda.collections.Lambda.Zadania.Zad3kolejne;

public class Student {

    private String firstName;
    private String lastName;
    private int age;
    private int id;

    public Student(String firstName, String lastName, int age, int id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return String.format("Nowy student: %s lat , indeks %d", firstName, age, id);
    }
}
