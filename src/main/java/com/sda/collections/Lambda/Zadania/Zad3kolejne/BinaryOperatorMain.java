package com.sda.collections.Lambda.Zadania.Zad3kolejne;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;

public class BinaryOperatorMain {
    public static void main(String[] args) {
        Person person1 = new Person("Krzysztof", "Krawczyk", 45, true);
        Person personJacek = new Person("Jacek", "Krawczyk", 12, true);
        Person person2 = new Person("Baba", "Odchlopa", 46, false);
        Person person3 = new Person("Mlody", "Ziomek", 14, true);
        List<Person> peopleList = Arrays.asList(person1, person2, person3, personJacek);
        List<Person> peopleList2 = Arrays.asList( personJacek,person3);

        //przyklad z odejmowaniem
        BinaryOperator<Integer> substract = (x, y)-> x-y;
        System.out.println(substract.apply(12,8));
        //8. Napisz funkcję, która przyjmuje dwie Person i zwraca osobę starszą.
        BinaryOperator<Person> getOlder = (x,y)->{
            if(x.getAge()>=y.getAge())return x;
            else return y;
        };

        System.out.println(getOlder.apply(person1,person2));

        /*
        9. Napisz funkcję, która przyjmuje dwie listy
         <Person> i zwraca listę <Person>, w której średnia wieku jest większa.
         */

        Function<List<Person>,Double> getAvgAge = x->{
            if(x.size()==0)return 0.0;
            double avg = 0;
            for (Person person : x) {
                avg+=person.getAge();
            }
            return avg/x.size();
        };

        BinaryOperator<List<Person>> getOlderGroup = (x,y)->{

            double xAvg = getAvgAge.apply(x);
            double yAvg = getAvgAge.apply(y);
            if(xAvg>=yAvg)return x;
            else return y;
        };

        System.out.println(getOlderGroup.apply(peopleList,peopleList2));

        //przyklad z Consumerem

        Consumer<Person> printAgeAndFirstName = x-> System.out.println(x.getAge()+" "+x.getFirstName());
        printAgeAndFirstName.accept(person1);
        printAgeAndFirstName.accept(person2);

//        String ageAndName = printAgeAndFirstName.accept(person1);

        //  ^ NIEPOPRAWNE! Consumer niczego nie zwraca
        Consumer<Person> birthday = x->{
            x.setAge(x.getAge()+1);
            System.out.println(x.getFirstName()+" has now "+x.getAge());
        };
        System.out.println(person1);
        birthday.accept(person1);
        System.out.println(person1);
    }
}