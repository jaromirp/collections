package com.sda.collections.Lambda.Zadania;

//PREDICATE
//1. Napisz funkcję, która przyjmuje liczbę i sprawdza, czy liczba ta jest parzysta.
//2. Napisz funkcję, która przyjmuje listę Stringów i sprawdza, czy wśród nich jest String zawierający słowo "gotchya".
//3. Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale. Napisz:
//	a) funkcję, która zwraca true, jeśli Person jest mężczyzną
//	b) funkcję, która zwraca true, jeśli Person ma na imię "Jacek"
//	c) funkcję, która zwraca true, jeśli jest osoba jest pełnoletnia.
//4.  Napisz funkcję, która zwraca true, jeśli w kolekcji <Person> znajduje się dorosły mężczyzna, który ma na imię jacek.

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class Zadanie3 {
    public static void main(String[] args) {

        //1. Napisz funkcję, która przyjmuje liczbę i sprawdza, czy liczba ta jest parzysta.
        Predicate<Integer> isEvenPredicate = x -> x % 2 == 0; //poniżej dla Liczby  Parzystej ma zwracać True lub False
        System.out.println(isEvenPredicate.test(4));  // dla liczby 4 zwróci True
        System.out.println(isEvenPredicate.test(3)); // dla liczby 3 zwróci False

        //2. Napisz funkcję, która przyjmuje listę Stringów i sprawdza, czy wśród nich jest String zawierający słowo "gotchya".
        List<String> lista = Arrays.asList("pierwsze zdanie; drugiezdanie; trzgotchyaeciezadanie".split(";"));

        System.out.println(lista.contains("gotcha"));
        System.out.println(lista.get(2).equals("gotcha"));

        Predicate<List<String>> containsStringContainingGotchya = list -> {
            for (String element : list) {

                if (element.contains( "gotchya")) return true;
            }
            return false;
        };
        System.out.println(containsStringContainingGotchya.test(lista));
    }
}