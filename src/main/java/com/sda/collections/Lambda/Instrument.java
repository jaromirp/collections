package com.sda.collections.Lambda;

public interface Instrument {

    void makeSound();

}
