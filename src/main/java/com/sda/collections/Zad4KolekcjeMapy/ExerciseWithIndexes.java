package com.sda.collections.Zad4KolekcjeMapy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ExerciseWithIndexes {
    public static void main(String[] args) {
        String text = "ala ma kota kota ma ala baba nie ma";

        String[] litery = text.split("");

        Map<String, Set<Integer>> map = new MyMap();

        Set<String> lettersInSet = new HashSet(Arrays.asList(litery));
        System.out.println(lettersInSet);
        for (String litera : lettersInSet) {
            Set<Integer> indexy = new HashSet<>();

            for (int i = 0; i < litery.length; i++) {
                if (litera.equals(litery[i])) {
                    indexy.add(i);
                }
            }

            map.put(litera, indexy);
        }
        map.remove(" ");

        System.out.println(map);

    }
}
