package com.sda.collections.Zad4KolekcjeMapy;

import java.util.HashMap;

import static javax.swing.UIManager.get;

//public class MyMap {
public class MyMap extends HashMap {

    @Override
    public String toString() {
        String result = "[";
        for (Object o : keySet()) {
            result += o + "->" + get(o) + ",";
        }
        result += "]";
        return result;
    }
}

