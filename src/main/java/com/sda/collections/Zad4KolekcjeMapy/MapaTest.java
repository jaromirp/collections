package com.sda.collections.Zad4KolekcjeMapy;

//1. Utwórz klasę MapaTest a w niej metodę psvm
// 2. Utwórz hashmapę gdzie kluczem będzie String – imię a wartościa int – wiek
// 3. Dodaj do mapy kilka wystąpień
// 4. Wyświetl mapę (sout)
// 5. Spróbuj dodać do mapy obecny już klucz z inna wartością – co się stanie?
// 6. Przeiteruj mapę za pomocą pętli for (.keySet())
// 7. Sprawdź zachowanie dla innych implementacji mapy: 1. LinkedHashMap 2. TreeMap

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapaTest {
    public static void main(String[] args) {
        //tutaj Map = names
        //Map<String, Integer> names = new ExerciseWithIndexes<>(); // Linia można wyświetlać jak poniżej //wyświetla losowo
       //Map<String, Integer> names = new TreeMap<>(); // Linia można wyświetlać jak poniżej //wyświetla kolejnosci Klucza alfabet albo liczba zależy co jest kluczem
       Map<String, Integer> names = new LinkedHashMap<>();  //wyświetla wg ustalonej kolejności jak poniżej
        String [] strings = {"jacek", "placek", "wacek", "jarek", "malgosia" }; //klucze

        Integer[] ints = {10,13,12,30, 9, 1}; //liczby
        for (int i = 0; i < strings.length; i++) {
            names.put(strings[i],ints[i]);

        }
        //drukujemy tylko te wartosci, których klucze zaczynaja sie na "j"
//        for (String name: names.keySet()) {
//            System.out.println(names.get(name));
//        }
//            names.values();
        //names.put ("placek", 18); // Można użyć zmiany dla Klucza=placek zmiana na wartość
        names.replace("placek", 21); //Mapa zmień
        System.out.println(names);
    }
}
