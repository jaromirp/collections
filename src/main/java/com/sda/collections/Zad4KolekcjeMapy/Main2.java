package com.sda.collections.Zad4KolekcjeMapy;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {

        Map<String, Integer> personMap = new LinkedHashMap<>();
       //Map<String, Integer> names = new LinkedHashMap<>();  //wyświetla wg ustalonej kolejności jak poniżej

        personMap.put("jacek", 18); //wrzucamy do mapy Klucz i wartość
        personMap.put("wacek", 10); //wrzucamy do mapy drugi Klucz i wartość
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj imię: jacek, wacek");
        String podajImie = scanner.nextLine();

        System.out.println(personMap.get(podajImie));

        System.out.println();
    }
}
