package com.sda.collections;
//List:
//
//Zad1.
//Stwórz listę Integerów. Wykonaj zadania:
//    - dodaj do listy 5 elementów ze scannera
//    - dodaj do listy 5 elementów losowych
//    - wypisz elementy
//Sprawdź działanie aplikacji.

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Zad1 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(); // to musisz zadeklarować dla lista.add Kolekcje

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Podaj 5 liczb");
//        int wejscie = scanner.nextInt();
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
            System.out.println("Podaj liczbe");
            int wejscie = scanner.nextInt();
            lista.add(wejscie );

        }

        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            int wylosowanaLiczba = random.nextInt(); // wylosowana liczba
            lista.add(wylosowanaLiczba);
        }

        System.out.println(lista);
    }
}
