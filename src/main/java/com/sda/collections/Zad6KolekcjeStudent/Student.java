package com.sda.collections.Zad6KolekcjeStudent;

////Stwórz aplikację, a w niej klasę Dziennik. Stwórz również klasę Student. Klasa Student powinna:
////    - posiadać listę ocen studenta (List<Double>)
////    - posiadać (pole) numer indeksu studenta (String)
////    - posiadać (pole) imię studenta
////    - posiadać (pole) nazwisko studenta

import java.util.ArrayList;
import java.util.List;

public class Student {

    private List<Double> listaOcenStudenta = new ArrayList<>(); // to musisz zadeklarować dla lista.add Kolekcje
    private String imieStudenta;
    private String nazwiskoStudenta;
    private String numerIndeksuStudenta;

    public void setListaOcenStudenta(List<Double> listaOcenStudenta) {
        this.listaOcenStudenta = listaOcenStudenta;
    }

    public void setImieStudenta(String imieStudenta) {
        this.imieStudenta = imieStudenta;
    }

    public void setNazwiskoStudenta(String nazwiskoStudenta) {
        this.nazwiskoStudenta = nazwiskoStudenta;
    }

    public void setNumerIndeksuStudenta(String numerIndeksuStudenta) {
        this.numerIndeksuStudenta = numerIndeksuStudenta;
    }


    public Student(List<Double> listaOcenStudenta, String imieStudenta, String nazwiskoStudenta, String numerIndeksuStudenta) {
        this.listaOcenStudenta = listaOcenStudenta;
        this.imieStudenta = imieStudenta;
        this.nazwiskoStudenta = nazwiskoStudenta;
        this.numerIndeksuStudenta = numerIndeksuStudenta;
    }


    public List<Double> getListaOcenStudenta() {
        return listaOcenStudenta;
    }

    public String getNumerIndeksuStudenta() {
        return numerIndeksuStudenta;
    }

    public String getImieStudenta() {
        return imieStudenta;
    }

    public String getNazwiskoStudenta() {
        return nazwiskoStudenta;
    }

}
